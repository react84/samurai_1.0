import React from 'react'
import { Dimmer, Loader, Image, Segment } from 'semantic-ui-react'

const LoaderC = () => (
    <>
        <Loader active size='medium' />
        <div>Loading ...</div>
    </>
)

export default LoaderC
