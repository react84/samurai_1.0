import React from 'react';
import styles from './Navbar.module.css'
import {NavLink} from "react-router-dom";

const Nav = () => (
  <nav className={styles.nav}>
    <div className={`${styles.item} + ${styles.active}`}>
      <NavLink to='/profile' activeClassName={styles.active}>Profile</NavLink>
    </div>
    <div className={styles.item}>
      <NavLink to='/dialogs' activeClassName={styles.active}>Messages</NavLink>
    </div>
    <div className={styles.item}>
      <NavLink to='/users' activeClassName={styles.active}>Users</NavLink>
    </div>
    <div className={styles.item}>
      <a>News</a>
    </div>
    <div className={styles.item}>
      <a>Music</a>
    </div>
         <div className={styles.item}>
      <a>Settings</a>
    </div>
 </nav>
)

export default React.memo(Nav);
