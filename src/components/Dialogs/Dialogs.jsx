import React from "react";
import styles from './Dialogs.module.css';
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";
import {Field, reduxForm} from "redux-form";
import {Textarea} from "../common/FormsControlls/ FormControls";
import {maxLengthCreator, requiredField} from "../../utils/validators";

const maxLength = maxLengthCreator(100);
const MessageForm = ({handleSubmit}) => (
    <form onSubmit={handleSubmit}>
        <div>
            <Field placeholder={'enter text'} component={Textarea} name={'newMessageBody'} validate={[requiredField, maxLength]}/>
        </div>
        <div><button>Send</button> </div>
    </form>
)

const MessageFormRedux = reduxForm({form: 'message'})(MessageForm)

const Dialogs = (props) => {
    const {dialogs, messages, sendMessage} = props;
    const onSubmit = (values) => {
        sendMessage(values.newMessageBody)
    }
    return (
        <div>
            <div className={styles.dialogs}>
                <div className={styles.dialogsItems}>
                    {dialogs?.map(item => <DialogItem name={item.name} id={item.id}/>)}

                </div>
                <div className={styles.messages}>
                    <div>
                        {messages?.map(item => <Message text={item.message} id={item.id} />)}
                    </div>
                    <MessageFormRedux onSubmit={onSubmit}/>
                </div>
            </div>
        </div>
    )
}

export default Dialogs;
