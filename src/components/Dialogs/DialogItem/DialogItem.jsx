import React from "react";
import styles from './../Dialogs.module.css';
import {NavLink} from "react-router-dom";

const DialogItems = ({name, id}) => {
    const path = "/dialogs/" + id;
    return (
        <div className={`${styles.dialog} + ${styles.active}`}>
            <NavLink to={path}>
                {name}
            </NavLink>
        </div>
    )
}

export default DialogItems;
