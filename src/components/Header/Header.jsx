import React from 'react';
import styles from './Header.module.css'
import {NavLink} from "react-router-dom";

const Header = (props) => (
  <header className={styles.header}>
    <img src='https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png' />
    <div className={styles.loginBlock}>
        {props.isAuth ? <div>
            {props.login}
            <button onClick={props.logout}>logout</button>
        </div> : <NavLink to={'/login'}>Login</NavLink>}

    </div>
  </header>
)

export default React.memo(Header);