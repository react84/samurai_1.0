import React from 'react';
import Profile from "./Profile";
import {connect} from "react-redux";
import {getUserProfile, getStatus, updateStatus} from "../../redux/profileReducer";
import withAuthRedirect from "../../hoc/withAuthRedirect";
import {compose} from "redux";

class ProfileContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const userId = this.props?.match?.params?.userId || this.props.userId;
        this.props.getUserProfile(userId);
        this.props.getStatus(userId);
    }

    render() {
        return <Profile {...this.props} />
    }
}


const mapStateToProps = (state) => {
    return ({
        userProfile: state.profileReducer.userProfile,
        status: state.profileReducer.status,
        userId: state.auth.id,
        isAuth: state.auth.isAuth,
    })
}

export default compose(connect(mapStateToProps, {getUserProfile, getStatus, updateStatus}), withAuthRedirect)(ProfileContainer);