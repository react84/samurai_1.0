import React from 'react';
import styles from './Post.module.css'

const Post = ({message, likesCount}) => (
  <div className={styles.item}>
  <img src='https://www.kinonews.ru/insimgs/2019/newsimg/newsimg87089.jpg' />
  {message}
  <div>{`likes: ${likesCount}`}</div>
</div>
)

export default React.memo(Post);
