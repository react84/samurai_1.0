import React from 'react';
import styles from './MyPosts.module.css'
import Post from './Post/Post';




const MyPosts = ({posts, updateNewPostText, newPostText, addPost}) => {
    const changeText = (event) => {
        updateNewPostText(event.target.value);
    }
    return (
        <div className={styles.postsBlock}>
            <div>
                <h3>my posts</h3>
                <div>
                    <div>
                        <textarea onChange={changeText} value={newPostText} onChange={changeText}/>
                    </div>
                    <div>
                        <button onClick={addPost}>add new post</button>
                    </div>

                </div>

            </div>
            <div className={styles.posts}>
                {posts?.map(item => <Post message={item.message} id={item.id} likesCount={item.likesCount}/>)}
            </div>

        </div>
    )
}

export default MyPosts;
