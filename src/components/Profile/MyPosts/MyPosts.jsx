import React from 'react';
import styles from './MyPosts.module.css'
import Post from './Post/Post';
import {Field, reduxForm} from "redux-form";
import {requiredField, maxLengthCreator} from "../../../utils/validators";
import {Textarea} from "../../common/FormsControlls/ FormControls";

const maxLength = maxLengthCreator(10);
const PostForm = ({handleSubmit}) => (
    <form onSubmit={handleSubmit}>
        <div><Field placeholder={'enter text'} component={Textarea} name={'newPostText'} validate={[requiredField, maxLength]}/></div>
        <div><button>Add new post</button> </div>
    </form>
)

const PostReduxForm = reduxForm({form: 'newPost'})(PostForm);

const MyPosts = ({posts, addPost}) => {
    const addNewPost = (values) => {
        addPost(values.newPostText)
    }
    return (
        <div className={styles.postsBlock}>
            <div>
                <h3>my posts</h3>


            </div>
            <div className={styles.posts}>
                {posts?.map(item => <Post message={item.message} id={item.id} likesCount={item.likesCount}/>)}
            </div>
            <PostReduxForm onSubmit={addNewPost} />
        </div>
    )
}

export default MyPosts;
