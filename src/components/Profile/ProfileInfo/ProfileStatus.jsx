import React from 'react';

const ProfileStatus = (props) => {
    const [editMode, setEditMode] = React.useState(false);
    const [status, setStatus] = React.useState('');

    React.useEffect(() => {
        setStatus(props.status)
    }, [props.status])

    const activateMode = () => setEditMode(true);
    const deactivateMode = () => {
        setEditMode(false);
        props.updateStatus(status);
    };

    return (
        <>
            {
                !editMode && props.status ? (
                    <div>
                        <span onDoubleClick={activateMode}>{props.status}</span>
                    </div>
                ) : (
                    <div>
                        <span onDoubleClick={activateMode}>{props.status}</span>
                        <input onBlur={deactivateMode} value={status} autoFocus={true} onChange={(e) => setStatus(e.target.value)}/>
                    </div>
                )
            }
        </>
    )
};

export default React.memo(ProfileStatus);