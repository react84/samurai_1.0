import React from 'react';
import styles from './ProfileInfo.module.css'
import ProfileStatus from "./ProfileStatus";

const ProfileInfo = ({profile, status, updateStatus}) => {
    return (
        <div>

            <div className={styles.descriptionBlock}>
                <div>{profile?.fullName}</div>
                {
                    profile?.photos?.large && (
                        <img src={profile.photos?.large} />
                    )
                }
                <ProfileStatus status={status} updateStatus={updateStatus}/>
            </div>
        </div>
    )
}

export default React.memo(ProfileInfo);
