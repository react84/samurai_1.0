import React from "react";
import styles from './Users.module.css'
import {NavLink} from "react-router-dom";

const Users = (props) => {
    const {users, unfollow, follow, pageSize, totalUsersCount, currentPage, onPageChanged} = props;
    const pagesCount = Math.ceil(totalUsersCount / pageSize)
    const pages = [];
    for (let i = 0; i < pagesCount; i++) {
        pages.push(i + 1)
    }
    return (
        <div>
            {
                pages.map(item => <span onClick={() => onPageChanged(item)} className={item === currentPage && styles.selectedPage}>{item}</span>)
            }
            {users.map(u => {
                return (
                    <div key={u.id}>
                       <span>
                        <div>
                            <NavLink to={`/profile/${u.id}`}>
                                <img className={styles.profilePhoto} src={u.photoUrl || 'https://cdnb.artstation.com/p/assets/images/images/023/541/587/original/janaeither-t-profile-gif.gif?1579549292'}/>
                            </NavLink>
                           </div>
                        <div>
                            {
                                u.followed ?
                                    <button disabled={props.followingInProgress.some((id) => id === u.id)} onClick={() => unfollow(u.id)}>unfollow</button> :
                                    <button disabled={props.followingInProgress.some((id) => id === u.id)}  onClick={() => follow(u.id)}>follow</button>
                            }
                        </div>
                    </span>
                        <span>
                        <span>
                            <div>{u.name}</div>
                            <div>{u.status}</div>
                        </span>
                        <span>
                            <div>{'u.location.country'}</div>
                            <div>{'u.location.city'}</div>
                        </span>
                    </span>
                    </div>
                )
            })}
        </div>
    )
}

export default React.memo(Users);
