import React from "react";
import {connect} from "react-redux";
import {
    follow, requestUsers, toggleFollowingInProgress,
    unfollow
} from "../../redux/usersReducer";
import Users from "./Users";
import Loader from "../Loader/Loader";
import {
    getCurrentPage,
    getFollowingInProgress,
    getIsFetching,
    getPageSize,
    getTotalUsersCount,
    getUsers
} from "../../redux/usersSelectors";

class UsersContainer extends React.Component {
    componentDidMount() {
        const {pageSize, currentPage} = this.props;
        this.props.getUsers(currentPage, pageSize)
    }

    onPageChanged = (pageNumber) => {
        this.props.getUsers(pageNumber, this.props.pageSize)
    }

    onFollow = (userId) => this.props.follow(userId);

    onUnFollow = (userId) => this.props.unfollow(userId);


    render() {
        const {users, followingInProgress, pageSize, totalUsersCount, currentPage, isFetch} = this.props;
        if (isFetch) {
            return <Loader />
        }
        return <Users
            followingInProgress={followingInProgress}
            users={users}
            unfollow={this.onUnFollow}
            follow={this.onFollow}
            pageSize={pageSize}
            totalUsersCount={totalUsersCount}
            currentPage={currentPage}
            onPageChanged={this.onPageChanged}
        />
}

}

const mapStateToProps = (state) => (
    {
        users: getUsers(state),
        isFetching: getIsFetching(state),
        pageSize: getPageSize(state),
        totalUsersCount: getTotalUsersCount(state),
        currentPage: getCurrentPage(state),
        followingInProgress: getFollowingInProgress(state),
    }
);

export default connect(mapStateToProps, {
    follow,
    unfollow,
    toggleFollowingInProgress,
    getUsers: requestUsers,
})(UsersContainer)


