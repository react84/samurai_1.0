import React from 'react';
import {Field, reduxForm} from "redux-form";
import {requiredField} from "../../utils/validators";
import {Input} from "../common/FormsControlls/ FormControls";
import {connect} from "react-redux";
import {login} from "../../redux/authReducer";
import {Redirect} from "react-router-dom";

import styles from '../common/FormsControlls/FormControls.module.css'

const LoginForm = (props) => {
    const {handleSubmit} = props;
    return (
        <form onSubmit={handleSubmit}>
            <div><Field placeholder={'email'} name={'email'} component={Input} validate={[requiredField]}/></div>
            <div><Field placeholder={'password'} name={'password'} component={Input} validate={[requiredField]}/></div>
            <div><Field type={'checkbox'} name={'rememberMe'}  component={'input'} />remember me</div>
            {props.error && <div className={styles.formError}>{props.error}</div>}
            <div>
                <button>Login</button>
            </div>
        </form>
    )
}

const LoginReduxForm = reduxForm({form: 'login'})(LoginForm)

const Login = (props) => {
    const onSubmit = ({email, password, rememberMe}) => {
        props.login(email, password, rememberMe)
    }

    if (props.isAuth) {
        return <Redirect to={'/profile'} />
    }
    return (
        <div>
            Login Page
            <LoginReduxForm onSubmit={onSubmit}/>
        </div>
    )
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})
export default connect(mapStateToProps, {login})(Login);