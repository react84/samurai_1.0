import React from "react";
import styles from './FormControls.module.css';


const FormControl = (props) => {
    const isError = props.meta.touched && props.meta.error;
    return (
        <div className={styles.formControl + ' ' + (isError ? styles.error : '')}>
            <div>
                {props.children}
            </div>
            {isError && <span>{props.meta.error}</span>}

        </div>
    )
}

export const Textarea = (props) => (
        <FormControl {...props}>
            <textarea {...props.input}/>
        </FormControl>
    )


export const Input = (props) => (
    <FormControl {...props}>
        <input {...props.input}/>
    </FormControl>
    )

