import './App.css';
import Header from './components/Header';
import Navbar from './components/Navbar/Navbar';
import Profile from './components/Profile';
import Dialogs from "./components/Dialogs";
import { Switch, Route} from "react-router-dom";
import Users from "./components/Users";
import Login from "./components/Login/Login";
import {connect} from "react-redux";
import {useEffect} from "react";
import {initializeApp} from "./redux/appReducer";
import Loader from "./components/Loader/Loader";


function App(props) {
    useEffect(() => {
        props.initializeApp()
    }, [])
    if (!props.init) {
        return <Loader />
    }
    return (
        <Switch>
            <div className="app-wrapper">
                <Header/>
                <Navbar/>
                <div className="app-wrapper-content">
                    <Route render={() => <Dialogs />} path={'/dialogs'}/>
                    <Route render={(props) => <Profile {...props}/>} path={'/profile/:userId?'}/>
                    <Route render={() => <Users />} path={'/users'}/>
                    <Route render={() => <Login />} path={'/login'}/>
                </div>
            </div>
        </Switch>
    );
}

const mapStateToProps = (state) => ({
    init: state.app.initialized
})
export default connect(mapStateToProps, {initializeApp})(App);
