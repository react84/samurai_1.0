const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';
const SEND_MESSAGE = 'SEND_MESSAGE';

const initialState = {
    messages: [
        {id: 0, message: 'message0'},
        {id: 1, message: 'message1'},
        {id: 2, message: 'message2'},
        {id: 3, message: 'message4'},
        {id: 4, message: 'message5'},
    ],
    dialogs: [
        {id: 0, name: 'name00'},
        {id: 1, name: 'name11'},
        {id: 2, name: 'name2'},
        {id: 3, name: 'name3'},
        {id: 4, name: 'name4'},
    ],
}
const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE:
            return {
                ...state,
                newMessageBody: '',
                messages: [...state.messages, {id: state.messages.length, message: action.newMessageBody}]
            };
        default:
            return state

    }
}

export const sendMessageCreator = (newMessageBody) => {
    return (
        { type: SEND_MESSAGE, newMessageBody}
    )
}

export default dialogsReducer;
