import dialogsReducer from "./dialogsReducer";
import profileReducer from "./profileReducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 0, message: 'text00', likesCount: 111},
                {id: 1, message: 'text1', likesCount: 11},
                {id: 2, message: 'text2', likesCount: 122},
            ],
            newPostText: 'newPostText',
        },
        dialogsPage: {
            messages: [
                {id: 0, message: 'message0'},
                {id: 1, message: 'message1'},
                {id: 2, message: 'message2'},
                {id: 3, message: 'message4'},
                {id: 4, message: 'message5'},
            ],
            dialogs: [
                {id: 0, name: 'name00'},
                {id: 1, name: 'name11'},
                {id: 2, name: 'name2'},
                {id: 3, name: 'name3'},
                {id: 4, name: 'name4'},
            ],
            newMessageBody: '',
        }
    },
    getState() {
      return this._state
    },
    _callSubscriber() {},
    addPost() {

    },
    updateNewPostText(newPostText) {

    },

}

export default store;

