import profileReducer, {addPostActionCreator} from "./profileReducer";

it('new post should be added', () => {
    // test data
    const action = addPostActionCreator('it-kamasutra.com');
    const state = {
        posts: [
            {id: 0, message: 'text00', likesCount: 111},
            {id: 1, message: 'text1', likesCount: 11},
            {id: 2, message: 'text2', likesCount: 122},
        ],
    }

    // action
    const newState = profileReducer(state , action)

    // expected
    expect(newState.posts.length).toBe(4);
    expect(newState.posts[3].message).toBe('it-kamasutra.com');
})
