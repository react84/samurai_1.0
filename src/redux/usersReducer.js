import {followAPI, userAPI} from "../api";

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const SET_TOTAL_USERS_COUNT = 'SET_TOTAL_USERS_COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';


const initialState = {
    users: [],
    pageSize: 5,
    totalUsersCount: 22,
    currentPage: 1,
    isFetching: true,
    followingInProgress: [],
}
const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FOLLOW:
            return {
                ...state,
                users: state.users.map((u, id) => {
                    if (u.id === action.userId) {
                        return {...u, followed: true}
                    }
                    return u;
                })
            }
            case UNFOLLOW:
                return {
                    ...state,
                    users: state.users.map((u, id) => {
                        if (u.id === action.userId) {
                            return {...u, followed: false}
                        }
                        return u;
                    })
                }
        case SET_USERS:
            return {...state, users: [...action.users]}
        case SET_CURRENT_PAGE:
            return {...state, currentPage: action.currentPage}
        case SET_TOTAL_USERS_COUNT:
            return {...state, totalUsersCount: action.totalUsersCount}
        case TOGGLE_IS_FETCHING:
            return {...state, isFetching: action.isFetching}
        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {...state, followingInProgress: !action.followingInProgress ? [state.followingInProgress.filter((id) => id !== action.userId)] : [...state.followingInProgress, action.userId]}
        default:
            return state

    }
}

export const confirmFollow = (userId) => {
    return (
        { type: FOLLOW, userId}
    )
}

export const confirmUnfollow = (userId) => {
    return (
        { type: UNFOLLOW, userId}
    )
}

export const setUsers = (users) => {
    return {type: SET_USERS, users}
}

export const setCurrentPage = (currentPage) => {
    return {type: SET_CURRENT_PAGE, currentPage}
}

export const setTotalUsersCount = (totalUsersCount) => {
    return {type: SET_TOTAL_USERS_COUNT, totalUsersCount}
}
export const toggleIsFetching = (isFetching) => {
    return {type: TOGGLE_IS_FETCHING, isFetching}
}

export const toggleFollowingInProgress = (userId, followingInProgress) => {
    return {type: TOGGLE_IS_FOLLOWING_PROGRESS, userId, followingInProgress}
}


export const requestUsers = (currentPage, pageSize) => (dispatch) => {
    dispatch(toggleIsFetching(true));
    dispatch(setCurrentPage(currentPage));
    userAPI.getUsers(currentPage, pageSize).then(data => {
        dispatch(setUsers(data.items))
        dispatch(setTotalUsersCount(data.totalCount));
        dispatch(toggleIsFetching(false));
    })
}

export const follow = (userId) => (dispatch) => {
    dispatch(toggleFollowingInProgress(userId, true));
    followAPI.follow(userId).then(data => {
        dispatch(toggleFollowingInProgress(userId, false));
        if (data.resultCode == 0) {
            dispatch(confirmFollow(userId));
        }
    })
}
export const unfollow = (userId) => (dispatch) => {
    dispatch(toggleFollowingInProgress(userId, true));
    followAPI.follow(userId).then(data => {
        dispatch(toggleFollowingInProgress(userId, false));
        if (data.resultCode == 0) {
            dispatch(confirmUnfollow(userId));
        }
    })
}
export default usersReducer;
