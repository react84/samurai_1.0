import {profileAPI, userAPI} from "../api";

const ADD_POST = 'ADD-POST';
const SET_USER_PROFILE = 'SET_USER_PROFILE';
const SET_USER_STATUS = 'SET_USER_STATUS';

const initialState = {
    posts: [
        {id: 0, message: 'text00', likesCount: 111},
        {id: 1, message: 'text1', likesCount: 11},
        {id: 2, message: 'text2', likesCount: 122},
    ],
    userProfile: null,
    status: '',
}
const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            return {
                ...state,
                newPostText: '',
                posts: [...state.posts, {
                    id: state.posts.length,
                    message: action.newPostText,
                    likesCount: 0,
                }]

            };
        case SET_USER_PROFILE:
            return {
                ...state,
                userProfile: action.userProfile,
            };
        case SET_USER_STATUS:
            return {
                ...state,
                status: action.status,
            };
        case SET_USER_PROFILE:
            return {
                ...state,
                userProfile: action.userProfile,
            };
        default:
            return state;

    }
}

export const addPostActionCreator = (newPostText) => {
    return {type: ADD_POST, newPostText}
}


export const setUserProfile = (userProfile) => {
    return {type: SET_USER_PROFILE, userProfile}
}

export const setUserStatus = (status) => {
    return {type: SET_USER_STATUS, status}
}

export const getUserProfile = (userId) => (dispatch) => {
    userAPI.getProfile(userId).then(data => {
        dispatch(setUserProfile(data))
    })
}
export const getStatus = (userId) => (dispatch) => {
    profileAPI.getStatus(userId).then(data => {
        dispatch(setUserStatus(data))
    })
}

export const updateStatus = (status) => (dispatch) => {
    profileAPI.updateStatus(status).then(data => {
        if (data.resultCode === 0) {
            dispatch(setUserStatus(status))
        }
    })
}


export default profileReducer;
