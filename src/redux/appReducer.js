import {authAPI} from "../api";
import {stopSubmit} from "redux-form";
import {getAuthUserData} from "./authReducer";

const INITIALIZED_SUCCESS = 'INITIALIZED_SUCCESS';

const initialState = {
    initialized: false,
}
const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALIZED_SUCCESS:
            console.log('appReducer', action)
            return {
                ...state,
                initialized: true,
            };
        default:
            return state

    }
}

export const initializedSuccess = () => {
    return (
        { type: INITIALIZED_SUCCESS }
    )
}

export const initializeApp = () => (dispatch) => {
    const result = dispatch(getAuthUserData());
    result.then(() => {
        dispatch(initializedSuccess());
    })


}

export default appReducer;
