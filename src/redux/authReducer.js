import {authAPI} from "../api";
import {stopSubmit} from "redux-form";

const SET_USER_DATA = 'SET_USER_DATA';

const initialState = {
    login: null,
    email: null,
    isFetching: '',
    isAuth: false,
}
const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.payload,
            };
        default:
            return state

    }
}

export const setAuthUserData = (id, login, email, isAuth) => {
    return (
        { type: SET_USER_DATA, payload: {id, login, email, isAuth}}
    )
}

export const getAuthUserData = () => (dispatch) => {
    return authAPI.getMe().then(data => {
        if (data.resultCode === 0) {
            const { id, login, email } = data.data;
            dispatch(setAuthUserData(id, login, email, true));
        }

    })
}

export const login = (email, password, rememberMe) => (dispatch) => {
    authAPI.login(email, password, rememberMe).then(data => {
        if (data.resultCode === 0) {
            dispatch(getAuthUserData());
        } else {
            dispatch(stopSubmit('login', {_error: data.messages.length > 0 ? data.messages[0] : 'some error'}))
        }

    })
}

export const logout = () => (dispatch) => {
    authAPI.logout().then(data => {
        if (data.resultCode === 0) {
            dispatch(setAuthUserData(null, null, null, false));
        }

    })
}

export default authReducer;
